### Oplossen van een binaire puzzl

deze scripts proberen een binaire puzzel op te lossen.

- binair.py    doet dit recursief,

- binair-2.py  probeert dit met de 'normale' substituties te doen, dwz '1 . 1' -> '1 0 1'