import numpy as np
import sys
from utils import flip_frame, read_frame
from pprint import pprint

count = 0
verify_in = 0
verify_out = 0
max_depth = 0

frame1 = read_frame('data-bin1.input')


def fill_first_free(frame, val):

    _frame = []
    replaced = False
    for rij in frame:

        if not replaced:
            if ' ' in rij:
                rij = rij.replace(' ', val, 1)
                replaced = True

        _frame.append(rij)

    return _frame


def valid_frame(frame):

    _frame = frame.copy()

    valid = True

    for line in _frame:

        if line.count('0') > len(line)/2:
            valid = False
            break
        elif line.count('1') > len(line)/2:
            valid = False
            break
        elif '000' in line:
            valid = False
            break
        elif '111' in line:
            valid = False



    _frame = flip_frame(_frame)

    for line in _frame:
        if line.count('0') > len(line)/2:
            valid = False
            break
        elif line.count('1') > len(line)/2:
            valid = False
            break
        elif '000' in line:
            valid = False
            break
        elif '111' in line:
            valid = False

    return valid


def found_it(frame):

    _frame = frame.copy()

    found = True
    for line in _frame:

        if line.count('0') != len(line)/2:
            found = False

        elif line.count('1') != len(line)/2:
            found = False


    if found:

        for line in flip_frame(_frame):

            if line.count('0') != len(line)/2:
                found = False

            if line.count('1') != len(line)/2:
                found = False

    return found


def try_frame(frame):
    '''
    check eerst of het frame een oplossing is. zo niet, probeer dan eerst een 'nul' op
    de eerste vrije positie, daarna een 'een'. voor beide waarden wordt dezelfde functie
    weer recursief aangeroepen
    '''

    global max_depth
    global count
    count += 1

    if not found_it(frame):

        if valid_frame(fill_first_free(frame, '0')):
            max_depth += 1
            try_frame(fill_first_free(frame, '0'))
            max_depth -= 1
        else:
            print(' !! frame niet geldig (0)')

        if valid_frame(fill_first_free(frame, '1')):
            max_depth += 1
            try_frame(fill_first_free(frame, '1'))
            max_depth -= 1
        else:
            print(' !! frame niet geldig (1)')


    else:

        print(f'!!!! gevonden !!!! ')
        print(f'pogingen         : {count}')
        print(f'gecheckte frames : {verify_in}')
        print(f'correcte frames  : {verify_out}')
        print(f'max depth        : {max_depth}')
        pprint(frame)

        sys.exit(0)



try_frame(frame1)
