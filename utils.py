def read_frame(input_file):

    frame = []

    with open(input_file, 'r') as f:
        for line in f:

            line = line.replace('\n', '').replace('|', '')

            frame.append(line)



    return frame


def apply_subs(_frame):

    _new_frame = []

    for rij in _frame:
        rij = rij.replace('1 1', '101')
        rij = rij.replace('0 0', '010')
        rij = rij.replace(' 11', '011')
        rij = rij.replace('11 ', '110')
        rij = rij.replace(' 00', '100')
        rij = rij.replace('00 ', '001')

        _new_frame.append(rij)

    return _new_frame


def flip_frame(_frame):

    flipped_frame = ['' for i in range(len(_frame[0]))]

    for i in range(len(_frame[0])):

        for j in range(len(_frame)):

            flipped_frame[i] += _frame[j][i]

    return flipped_frame


