from pprint import pprint
from utils import read_frame, apply_subs, flip_frame

import sys

test_frame = [
        '1  1 10 1 ',
        '1 1 0 1 10',
        '          ',
        '0         ',
        '          ',
        '          ',
        '          ',
        '          ',
        '          ',
        '          '
         ]

test_flipped = [
        '11 0      ',
        '          ',
        ' 1        ',
        '1         ',
        ' 0        ',
        '1         ',
        '01        ',
        '          ',
        '11        ',
        ' 0        '
]

assert test_flipped == flip_frame(test_frame)
assert test_frame == flip_frame(flip_frame(test_frame))

frame = read_frame('data-bin1.input')
pprint(frame)
new_frame = []

N = 200

done = False
while not (done):

    N -= 1
    if N<0:
        break

    new_frame = apply_subs(frame.copy())
    new_frame = flip_frame(new_frame.copy())
    new_frame = apply_subs(new_frame.copy())
    new_frame = flip_frame(new_frame.copy())

    print('\nframe')
    pprint(frame)

    print('\nnew_frame')
    pprint(new_frame)

    done = (frame == new_frame)
    frame = new_frame





